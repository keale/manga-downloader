package debug;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author jankiel
 */
public class Log {
    
    public static void p(String message) {
        System.out.println(message);
    }
    
    public static void p(Exception exception) {
        System.out.println(exception.getMessage());
    }
    
    public static void p(boolean result) {
        System.out.println(result);
    }
    
    public static void p(int number) {
        System.out.println(number);
    }
    
    public static void p(double number) {
        System.out.println(number);
    }
    
    public static void p(long number) {
        System.out.println(number);
    }
    
    private static SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    
    private static String infoLogFilename = "info.log";
    public static void logInfo(String message) {
        try(PrintWriter pen = new PrintWriter(
            new BufferedWriter(
                new FileWriter(infoLogFilename, true)))) {
        
            pen.println("[" + dateFormatter.format(new Date()) + "]: " + message);
            pen.close();
        } catch (IOException e) {
            Log.p(e);
        }
    }
    
    private static String errorLogFilename = "error.log";
    public static void logError(String message) {
        logInfo(message);
        try(PrintWriter pen = new PrintWriter(
            new BufferedWriter(
                new FileWriter(errorLogFilename, true)))) {
        
            pen.println("[" + dateFormatter.format(new Date()) + "]: " + message);
            pen.close();
        } catch (IOException e) {
            Log.p(e);
        }
    }
    
    private static String downloadLogFilename = "download.log";
    public static void logDownloadBegin(String title, String url) {
        String message = "Begin downloading \"" + title + "\"...";
        
        logInfo(url);
        logInfo(message);
        try(PrintWriter pen = new PrintWriter(
            new BufferedWriter(
                new FileWriter(downloadLogFilename, true)))) {
        
            pen.println("[" + dateFormatter.format(new Date()) + "]: " + url);
            pen.println("[" + dateFormatter.format(new Date()) + "]: " + message);
            pen.close();
        } catch (IOException e) {
            Log.p(e);
        }
    }
    
    public static void logDownloadEnd(String title, int numOfPages) {
        String message = "End downloading \"" + title + "\" with " + numOfPages + " pages.";
        
        logInfo(message);
        try(PrintWriter pen = new PrintWriter(
            new BufferedWriter(
                new FileWriter(downloadLogFilename, true)))) {
        
            pen.println("[" + dateFormatter.format(new Date()) + "]: " + message);
            pen.close();
        } catch (IOException e) {
            Log.p(e);
        }
    }
}
