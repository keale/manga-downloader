package downloaders;

import debug.Log;
import java.util.List;
import javax.swing.JLabel;
import javax.swing.SwingWorker;

/**
 *
 * @author jankiel
 */
public class Downloader extends SwingWorker<Void, String> {
    
    public static final int ONE_SECOND = 1000;
    public static final int TEN_SECONDS = 10 * ONE_SECOND;
    public static final int FIFTEEN_SECONDS = 15 * ONE_SECOND;
    public static final int THIRTY_SECONDS = 30 * ONE_SECOND;
    public static final int SIXTY_SECONDS = 60 * ONE_SECOND;

    public static final int ZERO_PERCENT_PROGRESS = 0;
    public static final int HUNDRED_PERCENT_PROGRESS = 100;
    
    protected String rootPath = "";
    protected String url;
    
    protected boolean hasEncounteredConnectionOrDownloadError;
    
    private OnDoneListener onDoneListener;
    
    protected String mangaTitle;
    
    protected JLabel titleLabel;
    protected JLabel progressLabel;
    
    public Downloader (String rootPath, String url,  JLabel titleLabel, 
        JLabel progressLabel) {
        //TODO add checking
        this.rootPath = rootPath;
        this.url = url;
        this.hasEncounteredConnectionOrDownloadError = false;
        
        this.titleLabel = titleLabel;
        this.progressLabel = progressLabel;
        
    }
    
    @Override
    protected Void doInBackground() throws Exception {
        return null;
    }

    @Override
    protected void done() {
        if (!hasEncounteredConnectionOrDownloadError) {
            progressLabel.setText(mangaTitle + " download successful.");
            setProgress(HUNDRED_PERCENT_PROGRESS);
        } else {
            progressLabel.setText(mangaTitle + " download failed.");
            setProgress(ZERO_PERCENT_PROGRESS);
        }
        
        onDoneListener.onDone();
    }

    @Override
    protected void process(List<String> stringArgs) {
        String textForTitleLabel = stringArgs.get(0);
        String textForProgressLabel = stringArgs.get(1);
        int progressForProgressBar = Integer.parseInt(stringArgs.get(2));
        
        if (textForTitleLabel != null) {
            titleLabel.setText(textForTitleLabel);
        }
        
        if (textForProgressLabel != null) {
            progressLabel.setText(textForProgressLabel);
               Log.logInfo(textForProgressLabel);
        }
        
        setProgress(progressForProgressBar);
    }
    
    protected static String getImageNameFromUrl(String url) {
        String[] urlComponents = url.split("\\\\|/");
        return urlComponents[urlComponents.length-1];
    }

    public OnDoneListener getOnDoneListener() {
        return onDoneListener;
    }

    public void setOnDoneListener(OnDoneListener onDoneListener) {
        this.onDoneListener = onDoneListener;
    }
}
