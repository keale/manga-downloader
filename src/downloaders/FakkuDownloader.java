package downloaders;

import java.io.File;

import javax.swing.JLabel;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import utils.FileOperations;
import utils.StringTransformations;
import debug.Log;

/**
 *
 * @author jankiel
 */
@Deprecated
public class FakkuDownloader extends Downloader {
    
    public FakkuDownloader (String rootPath, String url,  JLabel titleLabel, 
        JLabel progressLabel) {
        super(rootPath, url, titleLabel, progressLabel);
    }

    @Override
    protected Void doInBackground() throws Exception {
        Document doc = null;
        String urlToProcess = url;
        
        while (doc == null) {
            try {
                doc = Jsoup.connect(urlToProcess).timeout(SIXTY_SECONDS)
                        .userAgent("Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_2) "
                        + "AppleWebKit/537.36 (KHTML, like Gecko) "
                        + "Chrome/33.0.1750.152 Safari/537.36").get();

            } catch (Exception e) {
                hasEncounteredConnectionOrDownloadError = true;

                publish(null, "Problem encountered while connecting to server. " +
                        "Retrying...", String.valueOf(ZERO_PERCENT_PROGRESS));
                
                doc = null;
            }
        }
        
        mangaTitle = doc.select("meta[property=og:title]").attr("content");
        
        //brace yourself, kludgy solution incoming
        String docHtml = doc.toString();
        String[] docHtmlLines = docHtml.split("\n");
        String numOfPagesInHtml = "";
        
        int maxNumberOfPages;
        
        for (int i = 0; i < docHtmlLines.length; i++) {
            //The page for this certain manga indicates the max number of 
            //pages. We just have to get that in the div.
            //the structure looks like:
            //<div class="left">
            // Pages
            //</div> 
            //<div class="right">
            // 18 pages
            //</div> 
            //We'll look for the text stated above in sequence
            //using a very kludgy solution that I will write
            //knowing that in the future I will curse myself in doing so
            //this is so FUCKING contrived, please forgive me
            if (i+6 < docHtmlLines.length) {
                if (
                        docHtmlLines[i].contains("<div class=\"left\">") &&
                        docHtmlLines[i+1].contains("Pages") &&
                        docHtmlLines[i+2].contains("</div>") &&
                        docHtmlLines[i+3].contains("<div class=\"right\">") &&
                        docHtmlLines[i+5].contains("</div>")
                    ) {
                    numOfPagesInHtml = docHtmlLines[i+4];
                    break;
                }
            }
        }
        
        //then we get something like:
        //'<div class="right">18</b> pages</div>'
        //we just go through the String, get the first number,
        //and all the numbers after it until we get a space
        StringBuilder numOfPagesStringBuilder = new StringBuilder("");
        boolean hasFoundFirstNumber = false;
        for (int i = 0; i < numOfPagesInHtml.length(); i++) {
            char currentChar = numOfPagesInHtml.charAt(i);
            if (hasFoundFirstNumber && !Character.isDigit(currentChar)) {
                break;
            }
            if (Character.isDigit(currentChar)) {
                hasFoundFirstNumber = true;
                numOfPagesStringBuilder.append(currentChar);
            }
        }
        
        maxNumberOfPages = Integer.parseInt(numOfPagesStringBuilder.toString());
        
        //so in analyzing the docHtml String, I found that the 
        //String we need(the url of the images) can be found in the
        //og:image meta property of the head of the html. let's get that
        String dirtyMangaUrl = doc.select("meta[property=og:image]")
                .attr("content");
        
        //now we have something like:
        //https://t.fakku.net/images/manga/t/thepromise_e/thumbs/002.thumb.jpg
        //let's format and sanitize it
        //dirtyMangaUrl looks like:
        //"https://t.fakku.net/images/manga/t/thepromise_e/thumbs/002.thumb.jpg"
        //with or without the heading and trailing '"'. 
        //if they exist let's remove them first
        if (dirtyMangaUrl.startsWith("\"")) {
            dirtyMangaUrl = dirtyMangaUrl.substring(1);
        }
        if (dirtyMangaUrl.endsWith("\"")) {
            dirtyMangaUrl = dirtyMangaUrl.substring(0, dirtyMangaUrl.length()-1);
        }

        //lets split using '/' and exclude the last two components
        //we need to make the "thumbs/002.thumb.jpg" into "images/002.jpg"
        //i'm kinda afraid of using replaceAll("thumbs") here
        //that's why we are doing it in this way
        String[] dirtyMangaUrlComponents = dirtyMangaUrl.split("/");
        //imageFilename here looks like: 002.thumb.jpg
        //I think it's safe to use replace here now
        String imageFilename = 
            dirtyMangaUrlComponents[dirtyMangaUrlComponents.length-1];
        imageFilename = imageFilename.replaceFirst(".thumb.", ".");

        //now to combine them
        StringBuilder mangaUrlStringBuilder = new StringBuilder("");
        for (int j = 0; j < dirtyMangaUrlComponents.length-2; j++) {
            mangaUrlStringBuilder.append(dirtyMangaUrlComponents[j])
                    .append("/");
        }
        
        //we get only the baseUrl (without the '002.jpg')
        //so that we can append an incrementing variable to it
        mangaUrlStringBuilder.append("images/");
            String baseUrl = mangaUrlStringBuilder.toString();
        
        //the base url should now look like:
        //https://t.fakku.net/images/manga/t/thepromise_e/images/
        //we get the file format and the length of the file name number
        String fileNameNumberPart = imageFilename.split("\\.")[0];
        String fileFormatPart = imageFilename.split("\\.")[1];
        
        //now, all the pieces are complete, we can proceed with the download
        for (int pageNo = 1; pageNo <= maxNumberOfPages; pageNo++) {
        
            String urlToUse = baseUrl + StringTransformations.pad(pageNo, 
                    fileNameNumberPart.length()) + "." + fileFormatPart;
            
            String mangaFilePath = rootPath + File.separator
                    + FileOperations.sanitizeFileName(mangaTitle);
            Log.p(mangaFilePath);
            String imageName = getImageNameFromUrl(urlToUse);

            int progress = (int) (((double) pageNo 
                    /(double) maxNumberOfPages)*100);
            
            publish(mangaTitle, "Downloading \"" + mangaTitle + " - " 
                    + pageNo + "\"...", String.valueOf(progress));
                
            boolean downloadisSuccessful = false;
            
            while (!downloadisSuccessful) {
                downloadisSuccessful = FileOperations.downloadFileFromUrl(
                        urlToUse, mangaFilePath, imageName, 
                        FIFTEEN_SECONDS);

                if (!downloadisSuccessful) {
                    hasEncounteredConnectionOrDownloadError = true;
                    
                    publish(mangaTitle, "Problem encountered while "
                            + "downloading \"" + mangaTitle + " - " 
                            + pageNo + "\". Retrying...", 
                            String.valueOf(progress));
                } else {
                    /*publish(mangaTitle, "\"" + mangaTitle + " - " 
                            + (i+1) + "\" download successful!", 
                            String.valueOf(progress));*/
                }
            }
            
        }
        
        return null;
    }
}
