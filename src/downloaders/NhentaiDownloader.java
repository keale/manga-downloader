package downloaders;

import debug.Log;
import java.io.File;
import java.util.Random;
import javax.swing.JLabel;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import utils.FileOperations;
import utils.StringTransformations;

/**
 *
 * @author jankiel
 */
public class NhentaiDownloader extends Downloader {
    
    public NhentaiDownloader(String rootPath, String url, JLabel titleLabel, 
            JLabel progressLabel) {
        super(rootPath, url, titleLabel, progressLabel);
    }

    @Override
    protected Void doInBackground() throws Exception {
        Document doc = null;
        String urlToProcess = url;
        
        while (doc == null) {
            try {
                doc = Jsoup.connect(urlToProcess).timeout(SIXTY_SECONDS)
                        .userAgent("Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_2) "
                        + "AppleWebKit/537.36 (KHTML, like Gecko) "
                        + "Chrome/33.0.1750.152 Safari/537.36").get();

            } catch (Exception e) {
                Log.p(e);
                hasEncounteredConnectionOrDownloadError = true;

                publish(null, "Problem encountered while connecting to server. " +
                        "Retrying...", String.valueOf(ZERO_PERCENT_PROGRESS));
                
                doc = null;
            }
        }
        
        // just like fakku, the image url is also found on the metadata so let's save that
        String baseImageUrl = doc.select("meta[property=og:image]")
                .attr("content");
        
        //the url is like:
        //"https://t.nhentai.net/galleries/1520424/cover.jpg" or png
        //all we need to do is remove the last "cover.jpg"
        if (baseImageUrl.contains("cover.jpg")) {
            baseImageUrl = baseImageUrl.substring(0, baseImageUrl.lastIndexOf("cover.jpg")); 
        } else if (baseImageUrl.contains("cover.png")) {
            baseImageUrl = baseImageUrl.substring(0, baseImageUrl.lastIndexOf("cover.png")); 
        } else {
            publish(null, "Failed to process \"" + urlToProcess + "\". Skipping...", 
                    String.valueOf(ZERO_PERCENT_PROGRESS));
            Log.logError("Failed to process \"" + urlToProcess + "\". Skipping...");
        }
        
        //we also need to replace "https://t" to "https://i"
        baseImageUrl = baseImageUrl.replaceAll("t.nhentai.net", "i.nhentai.net");
        
        //manga details can be found inside a div with id "info"
        Element infoDiv = doc.select("#info").first();
        
        //first h1 element contains our title
        mangaTitle = infoDiv.select("h1").text();
        
        Log.logDownloadBegin(mangaTitle, url);
        
        int maxNumberOfPages = 1;
        
        String infoDivHtml = infoDiv.toString();
        String[] docHtmlLines = infoDivHtml.split("\n");
        for (int i = 0; i < docHtmlLines.length; i++) {
            //the page numbers exist in the next line after "Pages:"
            // in a format like <span><a><span> XXX
            if (docHtmlLines[i].contains("Pages:")) {
                String pagesLine = docHtmlLines[i+1].trim();
                
                Document pagesDoc = Jsoup.parseBodyFragment(pagesLine);
                Element pagesElement = 
                        pagesDoc.select("span").first()
                                .select("a").first()
                                .select("span").first();
                maxNumberOfPages = Integer.parseInt(pagesElement.html());
                break;
            }
        }
        
        //now let's start
        String[] extensions = {"jpg", "png"};
        Random mizer = new Random();
        for (int pageNo = 1; pageNo <= maxNumberOfPages; pageNo++) {
        
            boolean downloadisSuccessful = false;
            for (String ext : extensions) {
                String urlToUse = baseImageUrl + pageNo + "." + ext;
            
                String mangaFilePath = rootPath + File.separator
        	        + FileOperations.sanitizeFileName(mangaTitle);

                //String imageName = getImageNameFromUrl(urlToUse);
                String imageName = 
                    StringTransformations.pad(pageNo, 
                    String.valueOf(maxNumberOfPages).length())
                    + "." + ext;

                int progress = (int) (((double) pageNo 
                    /(double) maxNumberOfPages)*100);

                if (new File(mangaFilePath + File.separator + imageName).exists()) {
                    publish(mangaTitle, "Skipping \"" + mangaTitle + " - " 
                    + pageNo + "\"...", String.valueOf(progress));
                    downloadisSuccessful = true;
                    continue;
                }

                publish(mangaTitle, "Downloading \"" + mangaTitle + " - " 
                    + pageNo + "\"...", String.valueOf(progress));

                int maxRetries = 3;
                int numOfRetries = 0;

                while (!downloadisSuccessful) {
                    downloadisSuccessful = FileOperations.downloadFileFromUrl(
                        urlToUse, mangaFilePath, imageName, 
                        FIFTEEN_SECONDS);

                    if (!downloadisSuccessful) {
                    hasEncounteredConnectionOrDownloadError = true;

                    numOfRetries++;
                    if (numOfRetries > maxRetries) {
                        String message = "Failed to download \"" 
                            + mangaTitle + " - " + pageNo + "\". "
                            + "Skipping...";
                        publish(mangaTitle, message, String.valueOf(progress));

                        Log.logError(message);
                        Log.logError("Url: " + urlToUse);
                        Log.logError("Path: " + mangaFilePath);
                        Log.logError("Name: " + imageName);

                        break;
                    }

                    publish(mangaTitle, "Problem encountered while "
                        + "downloading \"" + mangaTitle + " - " 
                        + pageNo + "\". Retrying (" 
                        + numOfRetries + "/" + maxRetries + ")...", 
                        String.valueOf(progress));
                    } else {
                        /*publish(mangaTitle, "\"" + mangaTitle + " - " 
                            + (i+1) + "\" download successful!", 
                            String.valueOf(progress));*/
                    }
                }
        
                if (downloadisSuccessful) {
                    //let's rest for a random amount of time so as not to abuse the server
                    int randomMilliseconds = ONE_SECOND + mizer.nextInt(3 * ONE_SECOND);
                    publish(mangaTitle, "\"" + mangaTitle + " - " + pageNo 
                            + "\" download successful. Resting for " 
                            + randomMilliseconds + " ms...", String.valueOf(progress));
                    Thread.sleep(randomMilliseconds);

                    break; // from iterating over extensions

                }
                
            }
            
        }
        
        Log.logDownloadEnd(mangaTitle, maxNumberOfPages);
        
        return null;
    }
}
