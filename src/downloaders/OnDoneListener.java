package downloaders;

/**
 *
 * @author jankiel
 */
public interface OnDoneListener {
    public void onDone();
}