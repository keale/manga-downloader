package downloaders;

import debug.Log;
import java.io.File;
import javax.swing.JLabel;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import utils.FileOperations;

/**
 *
 * @author jankiel
 */
@Deprecated
public class PururinDownloader extends Downloader {
    
    public PururinDownloader (String rootPath, String url,  JLabel titleLabel, 
        JLabel progressLabel) {
        super(rootPath, url, titleLabel, progressLabel);
    }
    
    @Override
    protected Void doInBackground() throws Exception {
        Document doc = null;
        String urlToProcess = url;
        
        while (doc == null) {
            try {
                doc = Jsoup.connect(urlToProcess).timeout(SIXTY_SECONDS)
                        .userAgent("Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_2) "
                        + "AppleWebKit/537.36 (KHTML, like Gecko) "
                        + "Chrome/33.0.1750.152 Safari/537.36").get();

            } catch (Exception e) {
                Log.p(e);
                hasEncounteredConnectionOrDownloadError = true;

                publish(null, "Problem encountered while connecting to server. " +
                        "Retrying...", String.valueOf(ZERO_PERCENT_PROGRESS));
                
                doc = null;
            }
        }
        //get Manga Title from the title
        //we will get something like 
        //Title - Series H Doujinshi by Author - Pururin, Free Online Hentai Manga and Doujinshi Reader
        //we remove the "Pururin, Free Online Hentai Manga and Doujinshi Reader"
        String title = doc.title().substring(0, doc.title().length() - 
                "Pururin, Free Online Hentai Manga and Doujinshi Reader".length());
        
        //Now we get something like: Title - Series H Doujinshi by Author - 
        //we just get the title from here by looking for the 
        //string of characters before the firs " - "
        for (int i = 0; i < title.length(); i++) {
            if (i+2 < title.length()) {
                if (title.charAt(i) == ' '
                    && title.charAt(i+1) == '-'
                    && title.charAt(i+2) == ' ') {
                    mangaTitle = title.substring(0, i);
                    break;
                }
            }
        }
        
        //brace yourself, kludgy solution incoming
        String docHtml = doc.toString();
        String[] docHtmlLines = docHtml.split("\n");
        String numOfPagesInHtml = "";
        
        int maxNumberOfPages;
        
        for (int i = 0; i < docHtmlLines.length; i++) {
            //The page for this certain manga indicates the max number of 
            //pages. We just have to get that in the table it is in by looking
            //for '<td>Pages</td>'
            if (docHtmlLines[i].contains("<td>Pages</td>")) {
                numOfPagesInHtml = docHtmlLines[i+1];
                break;
            }
        }
        
        //then we get something like:
        //'              <td>20 (10.36 MB)</td>'
        //we just go through the String, get the first number,
        //and all the numbers after it until we get a space
        StringBuilder numOfPagesStringBuilder = new StringBuilder("");
        boolean hasFoundFirstNumber = false;
        for (int i = 0; i < numOfPagesInHtml.length(); i++) {
            char currentChar = numOfPagesInHtml.charAt(i);
            if (hasFoundFirstNumber && currentChar == ' ') {
                break;
            }
            if (Character.isDigit(currentChar)) {
                hasFoundFirstNumber = true;
                numOfPagesStringBuilder.append(currentChar);
            }
        }
        
        maxNumberOfPages = Integer.parseInt(numOfPagesStringBuilder.toString());
                    
        //now we can proceed to the images
        for (int pageNo = 1; pageNo <= maxNumberOfPages; pageNo++) {
            //so in here we process the original url and make it
            //into the page url (not the image url itself).
            //we change the 'gallery' into 'view', add 0 & a number (=pageNo-1)
            //after the 'mangaId', and an _ & a number (=pageNo) before the .html
            //in summary, the url that looks like this:
            //http://pururin.com/gallery/12248/stop-dont-be-an-escort.html
            //should look like this:
            //http://pururin.com/view/12248/00/stop-dont-be-an-escort_1.html
            //and at the end(at the maxNumberOfPages), should look like:
            //http://pururin.com/view/12248/019/stop-dont-be-an-escort_20.html
            //To do this, we split the string using '/'
            String[] urlComponents = urlToProcess.split("/");
            //we still need to insert an _number before the .html so we still
            //need to split the url and the .html for the last urlComponent
            String[] lastUrlComponentComponents = 
                    urlComponents[urlComponents.length-1].split("\\.");
            String lastComponent = lastUrlComponentComponents[0] + "_" + 
                    pageNo + "." + lastUrlComponentComponents[1];
            //now we merge them keeping in mind the rules stated above
            String pageUrl = 
                    urlComponents[0] + "/" + 
                    urlComponents[1] + "/" + 
                    urlComponents[2] + "/" + 
                    "view/" + 
                    urlComponents[4] + "/" + //this is the 'mangaId'
                    "0" + (pageNo-1)
                    + "/" + lastComponent;

            int progress = (int) (((double) (pageNo-1) 
                    /(double) maxNumberOfPages)*100);
            
            publish(mangaTitle, "Connecting to \"" + mangaTitle + " - " 
                    + pageNo + "\"...", String.valueOf(progress));
            
            Document pageDoc = null;
            
            while (pageDoc == null) {
                try {
                    //now we access the page url and save the main image
                    pageDoc = Jsoup.connect(pageUrl).timeout(SIXTY_SECONDS)
                        .userAgent("Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_2) "
                        + "AppleWebKit/537.36 (KHTML, like Gecko) "
                        + "Chrome/33.0.1750.152 Safari/537.36").get();

                } catch (Exception e) {
                    Log.p(e);
                    hasEncounteredConnectionOrDownloadError = true;

                    publish(null, "Problem encountered while connecting to \"" 
                            + mangaTitle + " - " + (pageNo) + "\". Retrying...", 
                            String.valueOf(progress));
                    
                    pageDoc = null;
                }
            }
        
            //so the main image is found in the link with class 'image-next'
            //let's get this link div by looking for the text starting with
            //'<a class="image-next"'
            String pageDocHtml = pageDoc.toString();
            String[] pageDocHtmlLines = pageDocHtml.split("\n");
            String linkTextWithMainImage = "";
            
            for (int i = 0; i < pageDocHtmlLines.length; i++) {
                if (pageDocHtmlLines[i].contains("<a class=\"image-next\"")) {
                    linkTextWithMainImage = pageDocHtmlLines[i];
                    break;
                }
            }
            
            String imageLink = "";
            //lets split the string by space, and look for the component
            //starting with 'src=' to get our image link.
            String[] linkTextWithMainImageComponents = 
                    linkTextWithMainImage.split(" ");
            for (String component : linkTextWithMainImageComponents) {
                if (component.startsWith("src=")) {
                    imageLink = component;
                    break;
                }
            }
            
            //we sanitize this link by removing the src=
            //and the heading and trailing '"'
            imageLink = imageLink.substring("src=".length());
            if (imageLink.startsWith("\"")) {
                imageLink = imageLink.substring(1);
            }
            if (imageLink.endsWith("\"")) {
                imageLink = imageLink.substring(0, imageLink.length()-1);
            }
            //then we insert the host (pururin.com) at the beginning of the
            //link to complete it.
            String imageUrl = "http://pururin.com" + imageLink;
            
            //now we can use it to download the image.
            String mangaFilePath = rootPath + File.separator
                    + FileOperations.sanitizeFileName(mangaTitle);
            String imageFilename = getImageNameFromUrl(imageUrl);

            progress = (int) (((double) pageNo 
                    /(double) maxNumberOfPages)*100);
            
            publish(mangaTitle, "Downloading \"" + mangaTitle + " - " 
                        + (pageNo) + "\"...", String.valueOf(progress));
            
            boolean downloadisSuccessful = false;
            
            while (!downloadisSuccessful) {
                downloadisSuccessful = FileOperations.downloadFileFromUrl(
                        imageUrl, mangaFilePath, imageFilename, FIFTEEN_SECONDS);
                
                if (!downloadisSuccessful) {
                    hasEncounteredConnectionOrDownloadError = true;
                    
                    publish(mangaTitle, "Problem encountered while "
                            + "downloading \"" + mangaTitle + " - " + 
                            (pageNo) + "\". Retrying...", 
                            String.valueOf(progress));
                } else {
                    /*publish(mangaTitle, "\"" + mangaTitle + " - " 
                            + (pageNo) + "\" download successful!", 
                            String.valueOf(progress));*/
                }
            }
        }
        
        return null;
    }
}
