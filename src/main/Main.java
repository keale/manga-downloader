package main;

import java.io.IOException;
import ui.MainJFrame;

/**
 *
 * @author jankiel
 */
public class Main {

    public static void main(String[] args) throws IOException {
        javax.swing.SwingUtilities.invokeLater(new Runnable() {
            
            @Override
            public void run() {
                MainJFrame main = new MainJFrame();
            }
        });
    }
}