package ui;

import debug.Log;
import downloaders.Downloader;
import downloaders.FakkuDownloader;
import downloaders.NhentaiDownloader;
import downloaders.OnDoneListener;
import downloaders.PururinDownloader;
import java.awt.Dimension;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.net.MalformedURLException;
import java.net.URL;
import javax.swing.BoxLayout;
import javax.swing.DefaultListModel;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.JScrollPane;
import javax.swing.ListSelectionModel;
import utils.DownloadQueue;


/**
 *
 * @author jankiel
 */
public class DownloadQueueJPanel extends JPanel 
    implements PropertyChangeListener, OnDoneListener {
    
    private static final long serialVersionUID = 1L;
    
    private static final int STATUS_WILL_START = 0;
    private static final int STATUS_HAS_STARTED = 1;
    private static final int STATUS_WILL_RESTART = 2;
    
    private static final String HOST_FAKKU = "www.fakku.net";
    private static final String HOST_PURURIN = "pururin.com";
    private static final String HOST_NHENTAI = "nhentai.net";
    
    private DownloadQueue<String> downloadQueue;
    private JList<String> jlist;
    private DefaultListModel<String> jlistModel;
    
    private JLabel currentlyDownloadingLabel;
    private JLabel progressLabel;
    private JProgressBar progressBar;
    
    private String rootPath;
    private String currentUrl;
    
    private int startStatus;
    
    public DownloadQueueJPanel(String rootPath) {
        downloadQueue = new DownloadQueue<>();
        
        this.rootPath = rootPath;
        startStatus = STATUS_WILL_START;
        
        //XXX ui still has problems
        setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
        
        JPanel currentPanel = new JPanel();
        currentPanel.setLayout(new BoxLayout(currentPanel, BoxLayout.Y_AXIS));
        
        currentlyDownloadingLabel = new JLabel("");
        currentlyDownloadingLabel.setHorizontalAlignment(JLabel.LEFT);
        currentPanel.add(currentlyDownloadingLabel);

        progressLabel = new JLabel("");
        progressLabel.setHorizontalAlignment(JLabel.LEFT);
        currentPanel.add(progressLabel);

        progressBar = new JProgressBar();
        progressBar.setMinimum(0);
        progressBar.setMaximum(100);
        progressBar.setStringPainted(true);
        progressBar.setIndeterminate(false);
        //progressBar.setPreferredSize(new Dimension(100, 24));
        currentPanel.add(progressBar);
        
        add(currentPanel);
        
        jlistModel = new DefaultListModel<>();
        jlist = new JList<>(jlistModel);
        jlist.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        jlist.setLayoutOrientation(JList.VERTICAL);
        jlist.setVisibleRowCount(-1);
        //jlist.setCellRenderer(new DownloadItemCellRenderer(downloadQueue));
        
        JScrollPane jScrollPane = new JScrollPane(jlist);
        jScrollPane.setPreferredSize(new Dimension(548, 400));
        add(jScrollPane);
        
        //TODO add stop, pause, skip, skip to end buttons
    }
    
    public void addToDownloadQueue(String url) {
        addToDownloadQueue(new String[] {url}, false);
    }
    
    public void addToDownloadQueueOnTop(String url) {
        addToDownloadQueue(new String[] {url}, true);
    }
    
    public void addToDownloadQueue(String[] urls) {
        addToDownloadQueue(urls, false);
    }
    
    public void addToDownloadQueueOnTop(String[] urls) {
        addToDownloadQueue(urls, true);
    }
    
    public void addToDownloadQueue(String[] urls, boolean willAddOnTop) {
        for (String url : urls) {
            if (willAddOnTop) {
                downloadQueue.enqueueOnTop(url);
                jlistModel.add(0, url);    
            } else {
                downloadQueue.enqueue(url);
                jlistModel.addElement(url);
            }
        }
        
        if (startStatus != STATUS_HAS_STARTED) {
            processQueue();
        }
    }
    
    public void clearUi() {
        currentlyDownloadingLabel.setText("");
        progressLabel.setText("Ready");
        progressBar.setValue(progressBar.getMinimum());
    }
    
    public void processQueue() {
        if (!downloadQueue.canDequeue()) {
            return;
        }
        
        try {
            currentUrl = downloadQueue.dequeue();
            jlistModel.remove(0);
            
            if (startStatus == STATUS_WILL_START) {
                currentlyDownloadingLabel.setText(currentUrl);
                progressLabel.setText("Connecting...");
                progressBar.setValue(progressBar.getMinimum());
            }
            
            URL url = new URL(currentUrl);
            Downloader downloader = null;
            
            //if fakku
            if (url.getHost().equals(HOST_FAKKU)) {
                //downloader = new FakkuDownloader(rootPath, 
                    //currentUrl, currentlyDownloadingLabel, progressLabel);
                                        
                            JOptionPane.showMessageDialog(null, "Fakku no longer supported. "
                                            + "Proceeding to the next url.", "Error", 
                                            JOptionPane.ERROR_MESSAGE);
                            clearUi();
                            processQueue();
                
            //if pururin
            } else if (url.getHost().equals(HOST_PURURIN)) {
                //downloader = new PururinDownloader(rootPath, 
                    //currentUrl, currentlyDownloadingLabel, progressLabel);
                            JOptionPane.showMessageDialog(null, "Pururin no longer supported. "
                                            + "Proceeding to the next url.", "Error", 
                                            JOptionPane.ERROR_MESSAGE);
                            clearUi();
                            processQueue();
                
            //if nhentai
            } else if (url.getHost().equals(HOST_NHENTAI)) {
                
                downloader = new NhentaiDownloader(rootPath, 
                    currentUrl, currentlyDownloadingLabel, progressLabel);
            
            //else the url must be invalid
            } else {
                throw new MalformedURLException();
            }
            
            downloader.addPropertyChangeListener(this);
            downloader.setOnDoneListener(this);
                
            downloader.execute();
            startStatus = STATUS_HAS_STARTED;
            
        } catch (Exception e) {
            Log.p(e);
            
            JOptionPane.showMessageDialog(null, "Invalid/Unsupported URL. "
                    + "Proceeding to the next url.", "Error", 
                    JOptionPane.ERROR_MESSAGE);
            clearUi();
            processQueue();
        }
    }
    
    //TODO
    public String getMangaTitle(String url) {
        return url;
    }

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        if (evt.getPropertyName().equals("progress")) {
            int progress = (Integer) evt.getNewValue();
                progressBar.setValue(progress);
        } 
    }

    @Override
    public void onDone() {
        progressLabel.setText("Done");

        startStatus = STATUS_WILL_START;
        processQueue();
    }
}