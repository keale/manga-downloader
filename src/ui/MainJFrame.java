package ui;

import debug.Log;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileReader;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import utils.StringTransformations;

/**
 *
 * @author jankiel
 */
public class MainJFrame extends JFrame {
    
    private static final long serialVersionUID = 1L;

    private final String ROOTH_PATH_PREFS_PREFIX = "rootPath=";
    
    private DownloadQueueJPanel downloadQueueJPanel;
    
    private JButton button;
    
    private String rootPath;
    private boolean hasDownloadStarted;
    
    private boolean isNhentai = false;
    
    public MainJFrame() {
        checkAndRetrievePreferences();
        hasDownloadStarted = false;
        
        setTitle("Manga Downloader");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        
        final JPanel mainPanel = new JPanel();
        mainPanel.setLayout(new BoxLayout(mainPanel, BoxLayout.Y_AXIS));
        
        JPanel inputPanel = new JPanel();
        
        final JLabel label = new JLabel("Url or File Path:");
        label.setHorizontalAlignment(JLabel.RIGHT);
        label.setPreferredSize(new Dimension(88, 24));
        inputPanel.add(label);
        
        final JTextField textField = new JTextField();
        textField.setPreferredSize(new Dimension(640, 24));
        inputPanel.add(textField);
        
        button = new JButton("Start");
        button.setPreferredSize(new Dimension(140, 24));
        button.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent event) {
                processUri(textField.getText());
                textField.setText("");
            }
        });
        
        inputPanel.add(button);
        
        JPanel checkboxPanel = new JPanel();
        checkboxPanel.setLayout(new FlowLayout(FlowLayout.LEFT));
        
        JCheckBox checkbox = new JCheckBox("Nhentai?");
        checkbox.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                if (isNhentai) {
                    isNhentai = false;
                    label.setText("Url or File Path:");
                    label.setPreferredSize(new Dimension(88, 24));
                    textField.setPreferredSize(new Dimension(640, 24));
                    mainPanel.repaint();
                } else {
                    isNhentai = true;
                    label.setText("Url or File Path: https://nhentai.net/g/");
                    label.setPreferredSize(new Dimension(228, 24));
                    textField.setPreferredSize(new Dimension(500, 24));
                    mainPanel.repaint();
                }
            }
        });
        
        checkboxPanel.add(checkbox);
        
        downloadQueueJPanel = new DownloadQueueJPanel(rootPath);
        
        mainPanel.add(inputPanel);
        mainPanel.add(checkboxPanel);
        mainPanel.add(downloadQueueJPanel);
        
        add(mainPanel);
        pack();
        setVisible(true);
        
        downloadQueueJPanel.clearUi();
    }
    
    private void checkAndRetrievePreferences() {
        File prefsFile = new File("prefs.ini");
        
        try {
            if (!prefsFile.exists()) {
                rootPath = JOptionPane.showInputDialog(null, "Enter the "
                            + "path where the manga will be saved.");
                while (rootPath == null || rootPath.equals("")
                        || !new File(rootPath).exists()) {
                    rootPath = JOptionPane.showInputDialog(null, "Empty or "
                            + "invalid path entered. (This information is "
                            + "required)\n Enter the path where the manga will "
                            + "be saved.", "Error", JOptionPane.ERROR_MESSAGE);
                }

                PrintWriter pen = new PrintWriter(prefsFile.getAbsolutePath());
                pen.println(ROOTH_PATH_PREFS_PREFIX + rootPath);
                pen.close();
            } else {
                Scanner paper = new Scanner(
                    new FileReader(prefsFile.getAbsolutePath()));
                
                while (paper.hasNextLine()) {
                    String line = paper.nextLine();
                    
                    if (line.startsWith(ROOTH_PATH_PREFS_PREFIX)) {
                        rootPath = line.substring(ROOTH_PATH_PREFS_PREFIX.length());
                    }
                    //other prefs
                }
                paper.close();
            }
        } catch (Exception e) {
            Log.p(e);
        }
    }
    
    public void processUri(String uri) {
        //if url
        if (uri.startsWith("http://") || uri.startsWith("https://")) {
            addToDownloadQueue(uri);
        //if file (it is considered file iff it passes this
        } else if (uri.startsWith("C:\\") || uri.startsWith("D:\\")) {
            try {
                Scanner paper = new Scanner(new FileReader(uri));
                List<String> urlList = new ArrayList<String>();
                while (paper.hasNextLine()) {
                    urlList.add(paper.nextLine());
                }
                paper.close();
                addToDownloadQueue(urlList.toArray(new String[urlList.size()]));
            } catch (Exception e) {
                Log.p(e);
            }
        //nuke codes
        } else if (isNhentai && StringTransformations.isNumeric(uri)) {
            uri = "https://nhentai.net/g/" + uri;
            addToDownloadQueue(uri);
        //otherwise, this might be url (e.g. pururin.com/blabla)
        } else {
            uri = "http://" + uri;
            addToDownloadQueue(uri);
        }
    }
    
    public void addToDownloadQueue(String url) {
        downloadQueueJPanel.addToDownloadQueue(url);
            
        if (!hasDownloadStarted) {
            hasDownloadStarted = true;
            button.setText("Add to Queue");
        }
    }
    
    public void addToDownloadQueue(String[] urls) {
        downloadQueueJPanel.addToDownloadQueue(urls);
            
        if (!hasDownloadStarted) {
            hasDownloadStarted = true;
            button.setText("Add to Queue");
        }
    }
}
