package utils;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author jankiel
 */
public class DownloadQueue<O> {
    
    private List<O> list;
    
    public DownloadQueue() {
        list = new ArrayList<>();
    }
    
    public void enqueue(O item) {
        list.add(item);
    }
    
    public O dequeue() throws DownloadQueueUnderflowException {
        O item = peek();
        list.remove(item);
        return item;
    }
    
    public O peek() throws DownloadQueueUnderflowException {
        if (!canDequeue()) {
            throw new DownloadQueueUnderflowException();
        }
        
        return list.get(0);
    }
    
    public O peek(int i) throws DownloadQueueUnderflowException {
        if (!canDequeue()) {
            throw new DownloadQueueUnderflowException();
        }
        
        return list.get(i);
    }
    
    public void enqueueOnTop(O item) {
        list.add(0, item);
    }
    
    public String printItems(String separator) {
        StringBuilder string = new StringBuilder("");
        for (O item : list) {
            string.append(item.toString()).append(separator);
        }
        return string.toString();
    }
    
    public String printItems() {
        return printItems(", ");
    }
    
    @Override
    public String toString() {
        return printItems();
    }
    
    public boolean canDequeue() {
        return list.size() > 0;
    }
}
