package utils;

/**
 *
 * @author jankiel
 */
public class DownloadQueueUnderflowException extends Exception {
    
    private static final long serialVersionUID = 1L;

    @Override
    public String getMessage() {
        return "DownloadQueue is empty. list.size() == 0";
    }
    
}
