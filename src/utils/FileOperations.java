package utils;

import debug.Log;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Arrays;

import org.jsoup.Connection;
import org.jsoup.Jsoup;

/**
 *
 * @author jankiel
 */
public class FileOperations {
    final static int[] illegalChars = {
        34, 60, 62, 124, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 
        14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 
        30, 31, 58, 42, 63, 92, 47, 46
    };
    static {
        Arrays.sort(illegalChars);
    }

    public static boolean downloadFileFromUrl(String url, String path, 
            String filename, int timeout) {
        try {
            Connection.Response response = Jsoup.connect(url).timeout(timeout)
                    .userAgent("Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_2) "
                    + "AppleWebKit/537.36 (KHTML, like Gecko) "
                    + "Chrome/33.0.1750.152 Safari/537.36").ignoreContentType(true)
                    .execute();
            
            byte[] imageBytes = response.bodyAsBytes();

            writeToFile(imageBytes, path, filename);
            return true;
        } catch (Exception e) {
            Log.p(e);
            return false;
        }
    }
    
    public static void writeToFile(byte[] array, String path, String filename) 
        throws FileNotFoundException, IOException {
    
        File filePath = new File(path);
        if (!filePath.exists()) {
            filePath.mkdirs();
        }
        
        try (FileOutputStream outputStream = new FileOutputStream(path 
                + File.separator + filename)) {
            outputStream.write(array);
        } catch (Exception e) {
            Log.p(e);
        }
    }
    
    public static String sanitizeFileName(String badFileName) {
        StringBuilder cleanName = new StringBuilder();
        for (int i = 0; i < badFileName.length(); i++) {
            int c = (int)badFileName.charAt(i);
            if (Arrays.binarySearch(illegalChars, c) < 0) {
                cleanName.append((char)c);
            }
        }
        return cleanName.toString();
    }
}
