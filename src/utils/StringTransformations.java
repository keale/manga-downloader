package utils;

public class StringTransformations {

    public static String pad(int number, int length) {
        String string = String.valueOf(number);
        
        if (length <= 1) {
            return string + "";
        }
        
        if(string.length() + 1 > length) {
            return string + "";
        }

        StringBuilder paddedString = new StringBuilder(string);
        while (paddedString.length() < length &&
                paddedString.length() + 1 <= length) {
            paddedString = paddedString.insert(0, "0");
        }

        return paddedString.toString();
    }
    
    public static boolean isNumeric(String str) {
        try {
            Integer.parseInt(str);
            return true;
        } catch(NumberFormatException e) {
            return false;
        }
    }
}
